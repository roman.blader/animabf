import json
import io

translation = {}


def baseActorDefinition():
    return {"templates": ["base"]}


def baseItemDefinition():
    return{"templates": ["baseItem"]}


def generateResistances(tipoBono, nombre):
    translation["resistance."+nombre] = nombre
    return {
        "nombre": nombre,
        "caracterista": tipoBono,
        "base": 0,
        "bono": 0,
        "especial": [],
        "total": []
    }


def generateAtribute(atributeName):
    translation["atribute."+atributeName] = atributeName
    return {
        "name": atributeName,
        "base": 5,
        "bono": 0,
        "modificadores": []
    }


def generateDefHabilitie(bono, nombre):
    return {
        "nombre": nombre,
        "bono": bono,
        "base": 0,
        "cat": 0,
        "bonus": [],
        "total": 0
    }


def generateKi(bono):
    return{
        "acubase": 0,
        "acuAdd": 0,
        "acuBonos": [],
        "acuTotal": 0,
        "kiBase": 0,
        "kiAdd": 0,
        "kiBonos": [],
        "kiTotal": 0
    }


def addItemTemplate(itmTemplates, nombre, baseitem):
    itmTemplates[nombre] = baseitem


def addItem(items, nombre, info, templates=[]):
    items[nombre]["templates"].extend(templates)
    for key, value in info.items():
        items[nombre][key] = value


character = {}

pj = baseActorDefinition()
pnj = baseActorDefinition()
criatura = baseActorDefinition()

# print(json.dumps(pj, indent=1))
# print(json.dumps(pnj, indent=1))
# print(json.dumps(criatura, indent=1))

template = {
    "Actor": {
        "types": ["pj", "pnj", "criatura"],
        "templates": {
            "base": {}
        }
    },
    "Item": {
        "types": ["arma", "armadura", "escudo", "objeto", "magia", "poderpsi", "tecnica", "marcialPasiva", "ventaja", "desventaja", "elan"],
        "templates": {
            "base": {
                "nombre": "",
                "descripcion": ""
            }
        }
    }
}
# Agrego las traducciones de los tipos de actor y de objeto
for name in template["Actor"]["types"]:
    translation[name] = name
    template["Actor"][name] = {"templates": ["base"]}
for name in template["Item"]["types"]:
    translation[name] = name
    template["Item"][name] = {"templates": ["base"]}

addItemTemplate(template["Item"]["templates"], "objeto", {
                "coste": 0, "peso": 0,"cantidad":1, "calidad": "normal"})
addItemTemplate(template["Item"]["templates"],
                "upgrade", {"coste": 0, "Efecto": ""})
addItemTemplate(template["Item"]["templates"], "combate", {
                "entereza": 0, "rotura": 0, "presencia": 0})

arma = {
    "dano": 0,
    "bonif": 0,
    "cac": True,
    "ataque": 0,
    "defensa": 0,
    "danofinal": 0,
    "bono": 0
}
addItem(template["Item"], "arma", arma, templates=["objeto", "combate"])
addItem(template["Item"], "escudo", {
        "bonoesq": 0}, templates=["objeto", "combate"])
armadura = {
    "ta": {
        "fil": 0,
        "con": 0,
        "pen": 0,
        "cal": 0,
        "ele": 0,
        "fri": 0,
        "ene": 0
    },
    "localizacion": "",
    "llArmadura": 0,
    "penarm": 0,
    "especial": []
}
addItem(template["Item"], "armadura", armadura,
        templates=["objeto", "combate"])

addItem(template["Item"], "objeto", {}, templates=["objeto"])


def addDificultyToSpell(mag, tipo):
    mag[tipo] = {"zeon": 0, "intreq": 0, "mantenimiento": 0, "efecto": ""}


magia = {
    "nivel": 0,
    "accion": "Activa",
    "tipo": "efecto",
    "mantenimiento": False
}
addDificultyToSpell(magia, "base")
addDificultyToSpell(magia, "intermedio")
addDificultyToSpell(magia, "avanzado")
addDificultyToSpell(magia, "arcano")
addItem(template["Item"], "magia", magia)

poderpsi = {
    "nivel": 1,
    "accion": "Activa",
    "tipo": "efecto",
    "mantenimiento": False,
    "tabla": {
        "d20": "",
        "d40": "",
        "d80": "",
        "d120": "",
        "d160": "",
        "d200": "",
        "d240": "",
        "d280": "",
        "d320": "",
        "d360": "",
        "d400": "",
        "d440": ""
    }
}

addItem(template["Item"], "poderpsi", poderpsi)
tecnica = {
    "nivel": 1,
    "cm": 0,
    "AGI": 0,
    "CON": 0,
    "FUE": 0,
    "DES": 0,
    "POD": 0,
    "VOL": 0,
    "efectos": ""
}
addItem(template["Item"], "tecnica", tecnica)
addItem(template["Item"], "marcialPasiva", {}, templates=["upgrade"])
addItem(template["Item"], "ventaja", {}, templates=["upgrade"])
addItem(template["Item"], "desventaja", {}, templates=["upgrade"])
elan = {
    "coste":0,
    "requerimiento":0,
    "activo": True
}
addItem(template["Item"], "elan", elan)

salvacionesList = {
    'CON': ['fisica', 'enfermedades', 'venenos'],
    'POD': ['magica'],
    'VOL': ['psiquica']
}


salvaciones = {}
for caraceristica, listaSalvaciones in salvacionesList.items():
    for salvacion in listaSalvaciones:
        salvaciones[salvacion] = generateResistances(caraceristica, salvacion)

character["salvaciones"] = salvaciones
character["vida"] = {
    'base': 0,
    'bcategoria': 0,
    'multiplos': 0,
    'total': 0
}
character['baseInfo'] = {
    'nombre': '',
    'categoria': '',
    'nivel': 0,
    'sexo': '',
    'raza': '',
    'pelo': '',
    'ojos': '',
    'pdesarrollo': 400,
    'apariencia': 1,
    'tamano': 10,
    'experiencia': 0,
    'siguientenivel': 0
}
character['cansancio'] = {
    'base': 0,
    'especial': 0,
    'gastado': 0,
    'penalizador': 0
}
character['movimiento'] = {
    'base': 0,
    'pen': 0,
    'bon': 0,
    'final': 0,
    'mturno': 0
}
character["elan"] = {
    "entidad": "",
    "nivel" : 0
}

attributes = ['AGI', 'CON', 'DES', 'FUE', 'INT', 'PER', 'POD', 'VOL']
atribList = {}
for atrib in attributes:
    atribList[atrib] = generateAtribute(atrib)
character["atribs"] = atribList

combatAbilities = {
    'ataque': 'DES',
    'parada': 'DES',
    'esquiva': 'AGI',
    'llarmadura': 'FUE'
}

combatHabilitiesList = {}
for nombre, bono in combatAbilities.items():
    translation["combate."+nombre] = nombre
    combatHabilitiesList[nombre] = generateDefHabilitie(bono, nombre)
character['combat'] = combatHabilitiesList
character['combat']['defensaActiva'] = "parada"

ki = ['AGI', 'CON', 'DES', 'FUE', 'POD', 'VOL']
kiHabis = {}
for bono in ki:
    kiHabis[bono] = generateKi(bono)
character["ki"] = kiHabis


conjurationAbs = {'atar': "POD", 'controlar': "POD",
                  'dominar': 'VOL', 'invocar': "POD"}
conjurationList = {}
for nombre, bono in conjurationAbs.items():
    translation["conjuracion."+nombre] = nombre
    conjurationList[nombre] = generateDefHabilitie(bono, nombre)

character["conjuracion"] = conjurationList

sobrenaturales = {
    'proymagica': {
        'base': 0,
        'caracteristica': 'DES',
        'bono': 0,
        'clase': 0,
        'modificadores': [],
        'total': 0,
    },
    'zeon': {
        'base': 0,
        'clase': 0,
        'gasto': 0,
        'total': 0
    },
    'ACT': {
        'base': 0,
        'multiplos': 0,
        'especial': 0
    },
    'innata': {
        'base': 0,
        'bonos': []
    },
    'maximos': {
        'x10': 0,
        'x20': 0,
        'x30': 0,
        'x40': 0,
        'x50': 0
    }
}
psiquicas = {
    'CV': {
        'base': 1,
        'bonoClase': 0.0,
        'desarrollo': 0,
        'total': 0,
        'gastados': 0
    },
    'Potencialpsi': {
        'base': 0,
        'bonos': []
    }
}
character["sobrenaturales"] = sobrenaturales
character["psiquicas"] = psiquicas

abilitieList = {
    'atleticas': [
        {'nombre': 'acrobacias', 'bono': 'AGI', 'penArm': True},
        {'nombre': 'atletismo', 'bono': 'AGI', 'penArm': True},
        {'nombre': 'montar', 'bono': 'AGI', 'penArm': False},
        {'nombre': 'nadar', 'bono': 'AGI', 'penArm': True},
        {'nombre': 'saltar', 'bono': 'FUE', 'penArm': True},
        {'nombre': 'trepar', 'bono': 'AGI', 'penArm': True}
    ],
    'vigor': [
        {'nombre': 'frialdad', 'bono': 'VOL', 'penArm': False},
        {'nombre': 'pfuerza', 'bono': 'FUE', 'penArm': True},
        {'nombre': 'resistirdolor', 'bono': 'VOL', 'penArm': False}
    ],
    'perceptivas': [
        {'nombre': 'advertir', 'bono': 'PER', 'penArm': False},
        {'nombre': 'buscar', 'bono': 'PER', 'penArm': False},
        {'nombre': 'rastrear', 'bono': 'PER', 'penArm': False}
    ],
    'intelectuales': [
        {'nombre': 'animales', 'bono': 'INT', 'penArm': False},
        {'nombre': 'ciencia', 'bono': 'INT', 'penArm': False},
        {'nombre': 'herbolaria', 'bono': 'INT', 'penArm': False},
        {'nombre': 'historia', 'bono': 'INT', 'penArm': False},
        {'nombre': 'medicina', 'bono': 'INT', 'penArm': False},
        {'nombre': 'memorizar', 'bono': 'INT', 'penArm': False},
        {'nombre': 'navegacion', 'bono': 'INT', 'penArm': False},
        {'nombre': 'ocultismo', 'bono': 'INT', 'penArm': False},
        {'nombre': 'tasacion', 'bono': 'INT', 'penArm': False},
        {'nombre': 'vmagica', 'bono': 'POD', 'penArm': False}
    ],
    'sociales': [
        {'nombre': 'estilo', 'bono': 'POD', 'penArm': False},
        {'nombre': 'intimidar', 'bono': 'VOL', 'penArm': False},
        {'nombre': 'liderazgo', 'bono': 'POD', 'penArm': False},
        {'nombre': 'persuasion', 'bono': 'PER', 'penArm': False}
    ],
    'subterfugio': [
        {'nombre': 'cerrajeria', 'bono': 'DES', 'penArm': False},
        {'nombre': 'disfraz', 'bono': 'DES', 'penArm': False},
        {'nombre': 'ocultarse', 'bono': 'PER', 'penArm': True},
        {'nombre': 'robo', 'bono': 'DES', 'penArm': False},
        {'nombre': 'sigilo', 'bono': 'AGI', 'penArm': True},
        {'nombre': 'tramperia', 'bono': 'DES', 'penArm': False},
        {'nombre': 'veneno', 'bono': 'INT', 'penArm': False}
    ],
    'creativas': [
        {'nombre': 'arte', 'bono': 'POD', 'penArm': False},
        {'nombre': 'baile', 'bono': 'AGI', 'penArm': True},
        {'nombre': 'forja', 'bono': 'DES', 'penArm': False},
        {'nombre': 'musica', 'bono': 'POD', 'penArm': False},
        {'nombre': 'trucomanos', 'bono': 'DES', 'penArm': False}
    ],
    'especial': []
}
abListByType = {}
for abilitieType, abilities in abilitieList.items():
    translation['habilidad.'+abilitieType] = abilitieType
    abList = {}
    for ab in abilities:
        translation['habilidad.'+abilitieType+'.'+ab['nombre']] = ab['nombre']
        abList[ab['nombre']] = {
            'traduccion': 'habilidad.'+abilitieType+'.'+ab['nombre'],
            'base': 0,
            'caracteristica': ab['bono'],
            'bono': 0,
            'modificadores': [],
            'subHabi': 0,
            'clase': 0,
            'penArm': ab['penArm'],
            'total': 0
        }
        pass
        abListByType[abilitieType] = abilitieList
    pass
character['habilidades'] = abilitieList

# print(json.dumps(translation, indent=1))

template["Actor"]["templates"]["base"] = character

fileTranslation = io.open(f'lang/es.json', encoding='utf-8', mode='w')
fileTranslation.write(json.dumps(translation, indent=1))

fileTemplate = io.open(f"template.json", encoding="utf-8", mode='w')
fileTemplate.write(json.dumps(template, indent=1))
