export const _getInitiativeFormula = function(combatant) {
    const actor = combatant.actor;
    const init = actor.atribs.AGI.bono + 20 + actor.atribs.DES.bono
  
    let nd = 1;
    let mods = "";
    // TODO buscar el active weapon.
  
    const parts = [`${nd}d100${mods}`, init];
  
    return parts.filter(p => p !== null).join(" + ");
  };